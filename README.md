# EternalTwin 

This repository contains all we know about the Motion Twin games. It does not contain any custom code. Just data backups and useful information.

For each game, we try to retrieve all the data available. If you have some files that we don't have, please post them. If you know some help site for a game, please add a link to it. If you know some information about a game (bugs, network protocols, trivia, etc.).

## Game list

- AlphaBounce
- Caféjeux
- Corporate Soccer
- CroqueMotel
- Dinocard
- Dinoparc
- Frutiparc
- Hammerfest
- Miniville
- MonLapin
- Naturalchimie
- Naturalchimie (Classic)
- Pioupiouz
- Socratomancie
- Twinoid
